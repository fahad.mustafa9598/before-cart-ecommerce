import firebase from 'firebase/app';
import 'firebase/firestore';
import 'firebase/auth';

const config = {
    apiKey: "AIzaSyA0fHiy0bS4BcfhcgnqfJtjoudVBq-ZEQ0",
    authDomain: "my-new-project-212507.firebaseapp.com",
    databaseURL: "https://my-new-project-212507.firebaseio.com",
    projectId: "my-new-project-212507",
    storageBucket: "my-new-project-212507.appspot.com",
    messagingSenderId: "120595030884",
    appId: "1:120595030884:web:97083bdc200247644f5071",
    measurementId: "G-TT28NNDK6T"
};

export const createUserProfileDocument = async (userAuth, additionalData) => {
    if (!userAuth) return;
    const userRef = firestore.doc(`users/${userAuth.uid}`);
    const snapShot = await userRef.get();
    console.log(snapShot);
    if (!snapShot.exists) {
        const { displayName, email } = userAuth;
        const createdAt = new Date();
        try {
            await userRef.set({
                displayName,
                email,
                createdAt
            })
        } catch (err) {
            console.log('Error', err.message);
        }
    }
    return userRef;
}

firebase.initializeApp(config);

export const auth = firebase.auth();
export const firestore = firebase.firestore();

const provider = new firebase.auth.GoogleAuthProvider();
provider.setCustomParameters({ prompt: 'select_account' });
export const signInWithGoogle = () => auth.signInWithPopup(provider);

export default firebase;